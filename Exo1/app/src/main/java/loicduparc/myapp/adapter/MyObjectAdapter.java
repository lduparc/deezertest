package loicduparc.myapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import loicduparc.myapp.R;
import loicduparc.myapp.model.MyObject;

public class MyObjectAdapter extends RecyclerView.Adapter<MyObjectAdapter.ViewHolder> {

    private List<MyObject> mDataset;

    public MyObjectAdapter(List<MyObject> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MyObjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_object_event_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (mDataset != null && position < mDataset.size()) {
            MyObject obj = mDataset.get(position);
            if (obj != null) {
                holder.title.setText(obj.getId());
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
        }
    }
}