package loicduparc.myapp.enums;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by loicduparc on 16/04/15.
 */
public enum Status implements Parcelable {

    STATUS_0,
    STATUS_1,
    STATUS_2,
    STATUS_3,
    STATUS_4;

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {
        @Override
        public Status createFromParcel(final Parcel source) {
            return Status.values()[source.readInt()];
        }

        @Override
        public Status[] newArray(final int size) {
            return new Status[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(ordinal());
    }
}
