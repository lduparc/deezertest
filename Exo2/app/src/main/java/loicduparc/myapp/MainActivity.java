package loicduparc.myapp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import loicduparc.myapp.enums.UserPermissions;
import loicduparc.myapp.events.LoginEvent;
import loicduparc.myapp.events.LogoutEvent;
import loicduparc.myapp.events.PwdLostEvent;
import loicduparc.myapp.events.RegisterEvent;
import loicduparc.myapp.listener.MyAuthServiceListener;
import loicduparc.myapp.preferences.Preferences;
import loicduparc.myapp.service.MyAuthService;
import loicduparc.myapp.service.NewWebOperations;

import com.leanplum.Leanplum;
import com.leanplum.Var;
import com.leanplum.activities.LeanplumActionBarActivity;
import com.leanplum.activities.LeanplumActivity;
import com.leanplum.annotations.Variable;
import com.leanplum.callbacks.VariablesChangedCallback;

public class MainActivity extends LeanplumActionBarActivity {

    public static Var<String> serviceOperationMode = Var.define("serviceOps", "OLD");

    private static final String TAG = MainActivity.class.getSimpleName();
    @InjectView(R.id.output)
    TextView mOutput;
    private IAuthService mAuthEndpoint;
    private MyAuthServiceListener.Stub mAuthServiceListener = new MyAuthServiceListener.Stub() {
        @Override
        public void onLogin(final LoginEvent event) throws RemoteException {
            handleLoginEvent(event);
        }

        @Override
        public void onPasswordLost(final PwdLostEvent event) throws RemoteException {
            handlePasswordLostEvent(event);
        }

        @Override
        public void onRegister(final RegisterEvent event) throws RemoteException {
            handleRegisterEvent(event);
        }

        @Override
        public void onLogout(final LogoutEvent event) throws RemoteException {
            handleLogoutEvent(event);
        }

    };
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "Service connection established");

            mAuthEndpoint = IAuthService.Stub.asInterface(service);
            try {
                mAuthEndpoint.addListener(mAuthServiceListener);
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to add listener", e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "Service connection closed");
        }
    };

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LIFE CYCLE
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        if (BuildConfig.DEBUG) {
            Leanplum.setAppIdForDevelopmentMode("app_WLm8dsONsdik7tLvuZzLBorVd1dqNnzAvR98ayhAmKg", "dev_xbglS0LzkjMTeuZtSU6R7rBceiyVcgg3cGeZp64dXfc");
        } else {
            Leanplum.setAppIdForProductionMode("app_WLm8dsONsdik7tLvuZzLBorVd1dqNnzAvR98ayhAmKg", "prod_ToQQl0r6ahXVdZlEEhw51tpBEbrbvp2V1P3OjuMQ988");
        }

        Leanplum.start(this);

        Leanplum.addVariablesChangedHandler(new VariablesChangedCallback() {
            @Override
            public void variablesChanged() {
                Log.i("A/B Testing", serviceOperationMode.stringValue());
                Preferences.setAbTested(MainActivity.this, serviceOperationMode.stringValue().equalsIgnoreCase(NewWebOperations.KEY));
            }
        });

        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopService();
        super.onDestroy();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * USER INTERFACE
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void initUI() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        mOutput.setText("");
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * UI EVENTS
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @OnClick(R.id.logout)
    void logout() {
        try {
            mAuthEndpoint.logout();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.signin)
    void signin() {
        try {
            mAuthEndpoint.signin("Email", "password");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.pwdlost)
    void passwordLost() {
        try {
            mAuthEndpoint.passwordLost();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.register)
    void register() {
        try {
            mAuthEndpoint.register("email@example.com", "password");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SERVICE
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void startService() {
        Intent serviceIntent = new Intent(MyAuthService.class.getName());
        serviceIntent.setPackage("loicduparc.myapp");
        startService(serviceIntent);

        bindService(serviceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    private void stopService() {
        try {
            mAuthEndpoint.removeListener(mAuthServiceListener);
            stopService(new Intent(MyAuthService.class.getName()));
            unbindService(mServiceConnection);
        } catch (Throwable ignore) {

        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SERVICE EVENTS
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void handleLoginEvent(final LoginEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<String> formattedPermissions = new ArrayList<>();
                for (UserPermissions permission : event.getUserProfile().getPermissions()) {
                    formattedPermissions.add(permission.name());
                }
                mOutput.append(event.getUserProfile().getFullname() + " [" + TextUtils.join(",", formattedPermissions) + "]\n\n");
            }
        });
    }

    private void handlePasswordLostEvent(final PwdLostEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mOutput.append(event.getMessage() + "\n\n");
            }
        });
    }

    private void handleRegisterEvent(final RegisterEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mOutput.append("Register StatusCode :" + event.getStatusCode() + "\n\n");
            }
        });
    }

    private void handleLogoutEvent(final LogoutEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mOutput.append("Logout" + "\n\n");
            }
        });
    }
}
