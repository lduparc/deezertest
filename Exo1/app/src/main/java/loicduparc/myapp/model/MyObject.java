package loicduparc.myapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import loicduparc.myapp.enums.Context;
import loicduparc.myapp.enums.Format;
import loicduparc.myapp.enums.Status;

/**
 * Created by loicduparc on 16/04/15.
 */
public class MyObject implements Parcelable {

    private String mId;
    private String mParentId;
    private int mDataCount;
    private int mFlags;
    private boolean mIsEnabled;
    private Date mLastUpdateDate;
    private Status mStatus; // Status is an enum
    private Context mContext; // Context is an enum
    private HashMap<Integer, Format> mAvailableFormats; // Format is an enum

    public MyObject() {
    }

    public MyObject(Parcel source) {
        this.mId = source.readString();
        this.mParentId = source.readString();
        this.mDataCount = source.readInt();
        this.mFlags = source.readInt();

        int enabledFlag = source.readInt();

        this.mIsEnabled = enabledFlag == 1;
        this.mLastUpdateDate = new Date(source.readLong());
        this.mStatus = source.readParcelable(Status.class.getClassLoader());
        this.mContext = source.readParcelable(Context.class.getClassLoader());

        final int size = source.readInt();
        this.mAvailableFormats = new HashMap<>(size);
        for (int i = 0; i < size; i++) {
            final int key = source.readInt();
            final Format format = source.readParcelable(Format.class.getClassLoader());
            this.mAvailableFormats.put(key, format);
        }
    }

    public String getId() {
        return this.mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getParentId() {
        return this.mParentId;
    }

    public void setParentId(String mParentId) {
        this.mParentId = mParentId;
    }

    public int getDataCount() {
        return this.mDataCount;
    }

    public void setDataCount(int mDataCount) {
        this.mDataCount = mDataCount;
    }

    public int getFlags() {
        return this.mFlags;
    }

    public void setFlags(int mFlags) {
        this.mFlags = mFlags;
    }

    public boolean IsEnabled() {
        return this.mIsEnabled;
    }

    public void setEnabled(boolean mIsEnabled) {
        this.mIsEnabled = mIsEnabled;
    }

    public Date getLastUpdateDate() {
        return mLastUpdateDate;
    }

    public void setLastUpdateDate(Date mLastUpdateDate) {
        this.mLastUpdateDate = mLastUpdateDate;
    }

    public Status getStatus() {
        return this.mStatus;
    }

    public void setStatus(Status mStatus) {
        this.mStatus = mStatus;
    }

    public Context getContext() {
        return this.mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public HashMap<Integer, Format> getAvailableFormats() {
        return this.mAvailableFormats;
    }

    public void setAvailableFormats(HashMap<Integer, Format> mAvailableFormats) {
        this.mAvailableFormats = mAvailableFormats;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<MyObject> CREATOR = new Creator<MyObject>() {
        @Override
        public MyObject createFromParcel(final Parcel source) {
            return new MyObject(source);
        }

        @Override
        public MyObject[] newArray(final int size) {
            return new MyObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mParentId);
        dest.writeInt(this.mDataCount);
        dest.writeInt(this.mFlags);
        dest.writeInt(this.mIsEnabled ? 1 : 0);
        dest.writeLong(this.mLastUpdateDate.getTime());
        dest.writeParcelable(this.mStatus, 0);
        dest.writeParcelable(this.mContext, 0);
        dest.writeInt(this.mAvailableFormats.size());

        for (Map.Entry<Integer, Format> entry : this.mAvailableFormats.entrySet()) {
            dest.writeInt(entry.getKey());

            final Format format = entry.getValue();
            dest.writeParcelable(format, 0);
        }
    }
}
