package loicduparc.myapp.utils;

import android.util.Log;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import loicduparc.myapp.ApplicationController;
import loicduparc.myapp.enums.Context;
import loicduparc.myapp.enums.Format;
import loicduparc.myapp.enums.Status;
import loicduparc.myapp.model.MyObject;

/**
 * Created by loicduparc on 16/04/15.
 */
public class Utils {

    private static Random sRandom = new Random();

    public static MyObject generateObject() {
        int rand = sRandom.nextInt(5);
        boolean isPair = rand % 2 == 0;
        HashMap<Integer, Format> formats = new HashMap<>();
        formats.put(0, Format.FORMAT_0);
        formats.put(1, isPair ? Format.FORMAT_2 : Format.FORMAT_1);
        formats.put(2, Format.FORMAT_4);

        MyObject object = new MyObject();
        object.setId(UUID.randomUUID().toString());
        object.setParentId(UUID.randomUUID().toString());
        object.setEnabled(isPair);
        object.setContext(Context.CONTEXT_0);
        object.setDataCount(isPair ? 3 : 2);
        object.setFlags(isPair ? 1 : 4);
        object.setLastUpdateDate(new Date());
        object.setStatus(isPair ? Status.STATUS_2 : Status.STATUS_4);
        object.setAvailableFormats(formats);
        return object;
    }

    public static List<MyObject> generateObjects() {
        List<MyObject> objects = new ArrayList<>();
        for (int idx = ApplicationController.NUM_OBJ_GEN; idx > 0; idx--) {
            objects.add(generateObject());
        }
        return objects;
    }
}
