package loicduparc.myapp.enums;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by loicduparc on 16/04/15.
 */
public enum Context implements Parcelable {

    CONTEXT_0,
    CONTEXT_1,
    CONTEXT_2,
    CONTEXT_3,
    CONTEXT_4;

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<Context> CREATOR = new Creator<Context>() {
        @Override
        public Context createFromParcel(final Parcel source) {
            return Context.values()[source.readInt()];
        }

        @Override
        public Context[] newArray(final int size) {
            return new Context[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(ordinal());
    }
}
