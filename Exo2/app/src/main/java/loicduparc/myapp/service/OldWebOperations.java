package loicduparc.myapp.service;

import java.util.UUID;

import loicduparc.myapp.events.LoginEvent;
import loicduparc.myapp.events.LogoutEvent;
import loicduparc.myapp.events.PwdLostEvent;
import loicduparc.myapp.events.RegisterEvent;
import loicduparc.myapp.model.NewUserProfile;
import loicduparc.myapp.model.OldUserProfile;

/**
 * Created by loicduparc on 18/04/15.
 */
public class OldWebOperations extends WebOperations {

    public static final String KEY = "OLD";

    public static WebOperations getInstance() {
        if (sInstance == null) {
            sInstance = new OldWebOperations();
        }
        return sInstance;
    }

    @Override
    LoginEvent login(String email, String password) {
        return new LoginEvent(new OldUserProfile("Duparc"), UUID.randomUUID().toString());
    }

    @Override
    PwdLostEvent passwordLost() {
        return new PwdLostEvent("PasswordLost from " + OldWebOperations.class.getSimpleName());
    }

    @Override
    RegisterEvent register(String email, String password) {
        return new RegisterEvent(0);
    }

    @Override
    LogoutEvent logout() {
        return new LogoutEvent(OldWebOperations.class.getSimpleName());
    }
}
