package loicduparc.myapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import loicduparc.myapp.IServiceObject;
import loicduparc.myapp.listener.MyObjectListener;
import loicduparc.myapp.model.MyObject;

public class MyService extends Service {

    private static final String TAG = MyService.class.getSimpleName();
    private List<MyObjectListener> mListeners = new ArrayList<>();
    private IServiceObject.Stub endpoint = new IServiceObject.Stub() {
    private Handler mHandler = new Handler();

        @Override
        public void addObject(final MyObject object) throws RemoteException {

            synchronized (mListeners) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (MyObjectListener listener : mListeners) {
                            try {
                                listener.handleObjectReceived(object);
                                listener.notifyDataSetChanged();
                            } catch (RemoteException e) {
                                Log.w(TAG, "Failed to notify listener " + listener, e);
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void addObjects(final List<MyObject> objects) throws RemoteException {

            synchronized (mListeners) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (MyObject object : objects) {
                            for (MyObjectListener listener : mListeners) {
                                try {
                                    listener.handleObjectReceived(object);
                                } catch (RemoteException e) {
                                    Log.w(TAG, "Failed to notify listener " + listener, e);
                                }
                            }
                        }

                        for (MyObjectListener listener : mListeners) {
                            try {
                                listener.notifyDataSetChanged();
                            } catch (RemoteException e) {
                                Log.w(TAG, "Failed to notify listener " + listener, e);
                            }
                        }
                    }
                });
            }

        }

        @Override
        public void addListener(MyObjectListener listener)
                throws RemoteException {

            synchronized (mListeners) {
                mListeners.add(listener);
            }
        }

        @Override
        public void removeListener(MyObjectListener listener)
                throws RemoteException {

            synchronized (mListeners) {
                mListeners.remove(listener);
            }
        }

    };

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (MyService.class.getName().equals(intent.getAction())) {
            return endpoint;
        }

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
