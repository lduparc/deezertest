package loicduparc.myapp.listener;

import loicduparc.myapp.events.LoginEvent;
import loicduparc.myapp.events.PwdLostEvent;
import loicduparc.myapp.events.RegisterEvent;
import loicduparc.myapp.events.LogoutEvent;

interface MyAuthServiceListener {

	void onLogin(in LoginEvent event);

	void onPasswordLost(in PwdLostEvent event);

	void onRegister(in RegisterEvent event);

	void onLogout(in LogoutEvent event);
}