package loicduparc.myapp.enums;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by loicduparc on 16/04/15.
 */
public enum Format implements Parcelable {

    FORMAT_0,
    FORMAT_1,
    FORMAT_2,
    FORMAT_3,
    FORMAT_4;

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Parcelable.Creator<Format> CREATOR = new Parcelable.Creator<Format>() {
        @Override
        public Format createFromParcel(final Parcel source) {
            return Format.values()[source.readInt()];
        }

        @Override
        public Format[] newArray(final int size) {
            return new Format[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(ordinal());
    }

}
