package loicduparc.myapp.model;

import android.os.Parcel;

import java.util.Arrays;
import java.util.List;

import loicduparc.myapp.enums.UserPermissions;

/**
 * Created by loicduparc on 18/04/15.
 */
public class OldUserProfile extends UserProfile {

    public OldUserProfile(String name) {
        super(name);
    }

    public OldUserProfile(Parcel source) {
        super(source);
    }

    @Override
    public List<UserPermissions> getPermissions() {
        return Arrays.asList(UserPermissions.SEND_FEEDBACK);
    }

    @Override
    public String getFullname() {
        return this.getName();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<OldUserProfile> CREATOR = new Creator<OldUserProfile>() {
        @Override
        public OldUserProfile createFromParcel(final Parcel source) {
            return new OldUserProfile(source);
        }

        @Override
        public OldUserProfile[] newArray(final int size) {
            return new OldUserProfile[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        super.writeToParcel(dest, flags);
    }
}
