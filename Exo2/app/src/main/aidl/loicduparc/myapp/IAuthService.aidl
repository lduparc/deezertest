// IAuthService.aidl
package loicduparc.myapp;

import loicduparc.myapp.events.LoginEvent;
import loicduparc.myapp.events.PwdLostEvent;
import loicduparc.myapp.events.RegisterEvent;
import loicduparc.myapp.events.LogoutEvent;
import loicduparc.myapp.listener.MyAuthServiceListener;

interface IAuthService {

    void signin(in String email, in String password);
    void logout();
    void passwordLost();
    void register(in String email, in String password);

	void addListener(MyAuthServiceListener listener);
	void removeListener(MyAuthServiceListener listener);

}
