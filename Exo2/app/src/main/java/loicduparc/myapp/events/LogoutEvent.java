package loicduparc.myapp.events;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by loicduparc on 18/04/15.
 */
public class LogoutEvent implements Parcelable {

    private String mWebServiceName;

    public LogoutEvent(String webserviceName) {
        this.mWebServiceName = webserviceName;
    }

    public LogoutEvent(Parcel source) {
        this.mWebServiceName = source.readString();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<LogoutEvent> CREATOR = new Creator<LogoutEvent>() {
        @Override
        public LogoutEvent createFromParcel(final Parcel source) {
            return new LogoutEvent(source);
        }

        @Override
        public LogoutEvent[] newArray(final int size) {
            return new LogoutEvent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.mWebServiceName);
    }
}
