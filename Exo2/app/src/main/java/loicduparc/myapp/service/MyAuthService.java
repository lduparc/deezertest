package loicduparc.myapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.leanplum.Leanplum;

import java.util.ArrayList;
import java.util.List;

import loicduparc.myapp.IAuthService;
import loicduparc.myapp.MainActivity;
import loicduparc.myapp.listener.MyAuthServiceListener;
import loicduparc.myapp.preferences.Preferences;

public class MyAuthService extends Service {

    private static final String TAG = MyAuthService.class.getSimpleName();
    private WebOperations mWebService;
    private List<MyAuthServiceListener> mListeners = new ArrayList<>();
    private IAuthService.Stub mEndpoint = new IAuthService.Stub() {

        private Handler mHandler = new Handler();

        @Override
        public void signin(final String email, final String pwd) throws RemoteException {

            synchronized (mListeners) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (MyAuthServiceListener listener : mListeners) {
                            try {
                                listener.onLogin(mWebService.login(email, pwd));
                            } catch (RemoteException e) {
                                Log.w(TAG, "Failed to notify listener " + listener, e);
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void logout() throws RemoteException {

            synchronized (mListeners) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (MyAuthServiceListener listener : mListeners) {
                            try {
                                listener.onLogout(mWebService.logout());
                            } catch (RemoteException e) {
                                Log.w(TAG, "Failed to notify listener " + listener, e);
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void passwordLost() throws RemoteException {

            synchronized (mListeners) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (MyAuthServiceListener listener : mListeners) {
                            try {
                                listener.onPasswordLost(mWebService.passwordLost());
                            } catch (RemoteException e) {
                                Log.w(TAG, "Failed to notify listener " + listener, e);
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void register(final String email, final String pwd) throws RemoteException {

            synchronized (mListeners) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (MyAuthServiceListener listener : mListeners) {
                            try {
                                listener.onRegister(mWebService.register(email, pwd));
                            } catch (RemoteException e) {
                                Log.w(TAG, "Failed to notify listener " + listener, e);
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void addListener(MyAuthServiceListener listener)
                throws RemoteException {

            synchronized (mListeners) {
                mListeners.add(listener);
            }
        }

        @Override
        public void removeListener(MyAuthServiceListener listener)
                throws RemoteException {

            synchronized (mListeners) {
                mListeners.remove(listener);
            }
        }

    };

    public MyAuthService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (MyAuthService.class.getName().equals(intent.getAction())) {
            initWebServiceOperator();
            return mEndpoint;
        }

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        mWebService = null;
        super.onDestroy();
    }

    private void initWebServiceOperator() {
        mWebService = Preferences.isABTested(this) ? NewWebOperations.getInstance() : OldWebOperations.getInstance();
    }
}
