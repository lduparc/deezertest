// IServiceObject.aidl
package loicduparc.myapp;

import loicduparc.myapp.model.MyObject;
import loicduparc.myapp.listener.MyObjectListener;

interface IServiceObject {

    void addObject(in MyObject object);
    void addObjects(in List<MyObject> objects);

	void addListener(MyObjectListener listener);
	void removeListener(MyObjectListener listener);

}
