package loicduparc.myapp;

import android.app.Application;
import android.util.Log;

import loicduparc.myapp.enums.Mode;

/**
 * Created by loicduparc on 17/04/15.
 */
public class ApplicationController extends Application {

    private static final long MAX_MEM_REF = 201326592L; //Max memory reference for new devices
    public static int NUM_OBJ_GEN = 400; //max num oj object MyObject generated every interval timeslot
    public static int PACK_INTERVAL = 200; //min timeslot interval to send objects
    public static int UNIT_INTERVAL = 20; //min timeslot interval to send objects
    private static ApplicationController sInstance;
    private IMemoryInfo iMemoryInfo;

    public static synchronized ApplicationController getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        configureDataSending();
    }

    private void configureDataSending() {
        long deviceMaxMem = Runtime.getRuntime().maxMemory();
        int percent = Math.round(deviceMaxMem * 100 / MAX_MEM_REF);
        int coef = 100 - percent;
        NUM_OBJ_GEN -= Math.round(NUM_OBJ_GEN * (coef/100f));
        PACK_INTERVAL += PACK_INTERVAL + Math.round((100f + percent)/PACK_INTERVAL);
    }

    public int getIntervalForMode(Mode mode) {
        return mode == Mode.PACK ? PACK_INTERVAL : UNIT_INTERVAL;
    }

    /**
     * @param level 05 - TRIM_MEMORY_RUNNING_MODERATE
     *              10 - TRIM_MEMORY_RUNNING_LOW
     *              15 - TRIM_MEMORY_RUNNING_CRITICAL
     *              20 - TRIM_MEMORY_UI_HIDDEN
     *              40 - TRIM_MEMORY_BACKGROUND
     *              60 - TRIM_MEMORY_MODERATE
     *              80 - TRIM_MEMORY_COMPLETE
     */

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        if (level == TRIM_MEMORY_RUNNING_CRITICAL || level == TRIM_MEMORY_RUNNING_LOW) {
            if (iMemoryInfo != null) {
                try {
                    iMemoryInfo.releaseMemory();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void registerMemoryListener(IMemoryInfo implementor) {
        iMemoryInfo = implementor;
    }

    public void unregisterMemoryListener() {
        iMemoryInfo = null;
    }

    interface IMemoryInfo {
        void releaseMemory();
    }
}
