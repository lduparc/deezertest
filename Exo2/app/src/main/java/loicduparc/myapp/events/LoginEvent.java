package loicduparc.myapp.events;

import android.os.Parcel;
import android.os.Parcelable;

import loicduparc.myapp.model.UserProfile;

/**
 * Created by loicduparc on 18/04/15.
 */
public class LoginEvent implements Parcelable {

    private UserProfile mUserProfile;
    private String mAuthToken;

    public LoginEvent(UserProfile profile, String token) {
        this.mUserProfile = profile;
        this.mAuthToken = token;
    }

    LoginEvent(Parcel source) {
        this.mUserProfile = source.readParcelable(UserProfile.class.getClassLoader());
        this.mAuthToken = source.readString();
    }

    public UserProfile getUserProfile() {
        return this.mUserProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.mUserProfile = userProfile;
    }

    public String getAuthToken() {
        return this.mAuthToken;
    }

    public void setAuthToken(String authToken) {
        this.mAuthToken = authToken;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<LoginEvent> CREATOR = new Creator<LoginEvent>() {
        @Override
        public LoginEvent createFromParcel(final Parcel source) {
            return new LoginEvent(source);
        }

        @Override
        public LoginEvent[] newArray(final int size) {
            return new LoginEvent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeParcelable(this.mUserProfile, 0);
        dest.writeString(this.mAuthToken);
    }
}
