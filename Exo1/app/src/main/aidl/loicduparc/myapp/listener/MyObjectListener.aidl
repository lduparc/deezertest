package loicduparc.myapp.listener;

import loicduparc.myapp.model.MyObject;

interface MyObjectListener {

	void handleObjectReceived(in MyObject object);
	void notifyDataSetChanged();
}