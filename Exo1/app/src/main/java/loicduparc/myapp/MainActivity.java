package loicduparc.myapp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import loicduparc.myapp.adapter.MyObjectAdapter;
import loicduparc.myapp.enums.Mode;
import loicduparc.myapp.listener.MyObjectListener;
import loicduparc.myapp.model.MyObject;
import loicduparc.myapp.service.MyService;
import loicduparc.myapp.utils.Utils;

public class MainActivity extends ActionBarActivity implements ApplicationController.IMemoryInfo {

    private static final String TAG = MainActivity.class.getSimpleName();
    private final long mStart = System.currentTimeMillis();
    private List<MyObject> mObjectEventList;
    private TextView mQuantityText;
    private RecyclerView mRecyclerView;
    private Mode mMode = Mode.UNIT;
    private int mTotalCount = 0;

    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(4);
    private Future<?> mFuture;


    private IServiceObject mEndpoint;

    private MyObjectListener.Stub mObjectListener = new MyObjectListener.Stub() {
        @Override
        public void handleObjectReceived(final MyObject object) throws RemoteException {
            updateUI(object);
        }

        @Override
        public void notifyDataSetChanged() throws RemoteException {
            notifyDataSetChangedUI();
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "Service connection established");

            mEndpoint = IServiceObject.Stub.asInterface(service);
            try {
                mEndpoint.addListener(mObjectListener);
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to add listener", e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "Service connection closed");
        }
    };
    private Runnable schedulerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!mFuture.isCancelled() && mEndpoint != null) {
                try {
                    if (mMode == Mode.UNIT) {
                        mEndpoint.addObject(Utils.generateObject());
                    } else {
                        mEndpoint.addObjects(Utils.generateObjects());
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LIFE CYCLE
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mQuantityText = (TextView) findViewById(R.id.quantity);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApplicationController.getInstance().registerMemoryListener(this);
        startService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService();
    }

    @Override
    protected void onStop() {
        try {
            ApplicationController.getInstance().unregisterMemoryListener();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        forceStopScheduler();
        super.onDestroy();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * USER INTERFACE
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void initUI() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        mObjectEventList = new ArrayList<>();
        mQuantityText.setText(getString(R.string.no_object));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);

        MyObjectAdapter adapter = new MyObjectAdapter(mObjectEventList);
        mRecyclerView.setAdapter(adapter);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SERVICE
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void startService() {
        Intent serviceIntent = new Intent(MyService.class.getName());
        serviceIntent.setPackage("loicduparc.myapp");
        startService(serviceIntent);

        bindService(serviceIntent, mServiceConnection, BIND_AUTO_CREATE);
        restartScheduler();
    }

    private void stopService() {
        try {
            stopScheduler();
            mEndpoint.removeListener(mObjectListener);
            stopService(new Intent(MyService.class.getName()));
            unbindService(mServiceConnection);
        } catch (Throwable ignore) {

        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * EVENTS
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void updateUI(MyObject object) {
        mObjectEventList.add(object);
        ++mTotalCount;
    }

    private void notifyDataSetChangedUI() {
        checkMemory();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRecyclerView != null && mRecyclerView.getAdapter() != null) {
                    mRecyclerView.getAdapter().notifyDataSetChanged();
                }

                if (mQuantityText != null) {
                    String formattedCount = getResources().getQuantityString(R.plurals.object_quantity, mTotalCount, mTotalCount);
                    mQuantityText.setText(formattedCount + " en " + ((System.currentTimeMillis() - mStart) / 1000) + " sec");
                }
            }
        });
    }

    @Override
    public void releaseMemory() {
        stopService();
        forceStopScheduler();
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SCHEDULER
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    private void restartScheduler() {
        if (mScheduler != null && !mScheduler.isShutdown()) {
            mFuture = mScheduler.scheduleAtFixedRate(schedulerRunnable, 0, ApplicationController.getInstance().getIntervalForMode(mMode), TimeUnit.MILLISECONDS);
        }
    }

    private void stopScheduler() {
        if (mFuture != null) {
            mFuture.cancel(true);
            Log.d(TAG, "stopScheduler");
        }
    }

    private void forceStopScheduler() {
        mScheduler.shutdownNow();
        mFuture = null;
        Log.d(TAG, "forceStopScheduler");
    }

    private void checkMemory() {
        long maxMem = Runtime.getRuntime().maxMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();

        int percent = (int) (totalMemory * 100 / maxMem);
        if (percent >= 95) {
            Log.d(TAG, "checkMemory at " + percent + "%");
            releaseMemory();
        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * MENUS
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_unit:
                setUnitMode();
                return true;
            case R.id.menu_group:
                setPackMode();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUnitMode() {
        mMode = Mode.UNIT;
        if (mFuture != null) {
            mFuture.cancel(true);
        }
        restartScheduler();
    }

    private void setPackMode() {
        mMode = Mode.PACK;
        if (mFuture != null) {
            mFuture.cancel(true);
        }
        restartScheduler();
    }
}
