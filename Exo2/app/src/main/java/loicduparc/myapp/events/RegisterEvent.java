package loicduparc.myapp.events;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by loicduparc on 18/04/15.
 */
public class RegisterEvent implements Parcelable {

    private int mStatusCode;

    public RegisterEvent(int statusCode) {
        this.mStatusCode = statusCode;
    }

    RegisterEvent(Parcel source) {
        this.mStatusCode = source.readInt();
    }

    public int getStatusCode() {
        return this.mStatusCode;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<RegisterEvent> CREATOR = new Creator<RegisterEvent>() {
        @Override
        public RegisterEvent createFromParcel(final Parcel source) {
            return new RegisterEvent(source);
        }

        @Override
        public RegisterEvent[] newArray(final int size) {
            return new RegisterEvent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(this.mStatusCode);
    }
}
