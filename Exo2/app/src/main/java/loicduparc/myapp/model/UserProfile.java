package loicduparc.myapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.UUID;

import loicduparc.myapp.enums.UserPermissions;

/**
 * Created by loicduparc on 18/04/15.
 */
public abstract class UserProfile implements Parcelable {

    protected static final String TAG = UserProfile.class.getSimpleName();

    protected String mId;
    protected String mName;

    protected UserProfile(String name) {
        this.mId = UUID.randomUUID().toString();
        this.mName = name;
    }

    protected UserProfile(Parcel source) {
        this.mId = source.readString();
        this.mName = source.readString();
    }

    public abstract String getFullname();

    public abstract List<UserPermissions> getPermissions();

    public String getId() {
        return this.mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mName);
    }

}
