package loicduparc.myapp.enums;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by loicduparc on 18/04/15.
 */
public enum UserPermissions implements Parcelable {

    SEND_FEEDBACK,
    CONTACT_DEEZER,
    ACCESS_LYRICS;

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<UserPermissions> CREATOR = new Creator<UserPermissions>() {
        @Override
        public UserPermissions createFromParcel(final Parcel source) {
            return UserPermissions.values()[source.readInt()];
        }

        @Override
        public UserPermissions[] newArray(final int size) {
            return new UserPermissions[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(ordinal());
    }
}
