package loicduparc.myapp.service;

import java.util.UUID;

import loicduparc.myapp.events.LoginEvent;
import loicduparc.myapp.events.LogoutEvent;
import loicduparc.myapp.events.PwdLostEvent;
import loicduparc.myapp.events.RegisterEvent;
import loicduparc.myapp.model.NewUserProfile;

/**
 * Created by loicduparc on 18/04/15.
 */
public class NewWebOperations extends WebOperations {

    public static final String KEY = "NEW";

    public static WebOperations getInstance() {
        if (sInstance == null) {
            sInstance = new NewWebOperations();
        }
        return sInstance;
    }

    @Override
    LoginEvent login(String email, String password) {
        return new LoginEvent(new NewUserProfile("Duparc", email, "Loic"), UUID.randomUUID().toString());
    }

    @Override
    PwdLostEvent passwordLost() {
        return new PwdLostEvent("PasswordLost from " + NewWebOperations.class.getSimpleName());
    }

    @Override
    RegisterEvent register(String email, String password) {
        return new RegisterEvent(1);
    }

    @Override
    LogoutEvent logout() {
        return new LogoutEvent(NewWebOperations.class.getSimpleName());
    }
}
