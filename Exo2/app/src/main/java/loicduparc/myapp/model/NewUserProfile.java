package loicduparc.myapp.model;

import android.os.Parcel;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import loicduparc.myapp.enums.UserPermissions;

/**
 * Created by loicduparc on 18/04/15.
 */
public class NewUserProfile extends UserProfile {

    private String mEmail;
    private String mSurname;

    public NewUserProfile(String name, String email, String surname) {
        super(name);
        this.mEmail = email;
        this.mSurname = surname;
    }

    public NewUserProfile(Parcel source) {
        super(source);
        this.mEmail = source.readString();
        this.mSurname = source.readString();
    }

    public String getEmail() {
        return this.mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getSurname() {
        return TextUtils.isEmpty(this.mSurname) ? "" : this.mSurname;
    }

    public void setSurname(String surname) {
        this.mSurname = surname;
    }

    @Override
    public String getFullname() {
        return this.getName() + " " + getSurname();
    }

    @Override
    public List<UserPermissions> getPermissions() {
        return Arrays.asList(UserPermissions.SEND_FEEDBACK, UserPermissions.CONTACT_DEEZER, UserPermissions.ACCESS_LYRICS);
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<NewUserProfile> CREATOR = new Creator<NewUserProfile>() {
        @Override
        public NewUserProfile createFromParcel(final Parcel source) {
            return new NewUserProfile(source);
        }

        @Override
        public NewUserProfile[] newArray(final int size) {
            return new NewUserProfile[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.mEmail);
        dest.writeString(this.mSurname);
    }

}
