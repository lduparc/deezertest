package loicduparc.myapp.service;

import loicduparc.myapp.events.LoginEvent;
import loicduparc.myapp.events.LogoutEvent;
import loicduparc.myapp.events.PwdLostEvent;
import loicduparc.myapp.events.RegisterEvent;

/**
 * Created by loicduparc on 18/04/15.
 */
public abstract class WebOperations {

    protected static final String TAG = WebOperations.class.getSimpleName();

    protected static WebOperations sInstance = null;

    protected String mBaseUrl;

    public String getBaseUrl() {
        return this.mBaseUrl;
    }

    public void setBaseUrl(String url) {
        this.mBaseUrl = url;
    }

    abstract LoginEvent login(String email, String password);
    abstract PwdLostEvent passwordLost();
    abstract RegisterEvent register(String email, String password);
    abstract LogoutEvent logout();

}
