package loicduparc.myapp.events;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by loicduparc on 18/04/15.
 */
public class PwdLostEvent implements Parcelable {

    private String mMessage;

    public PwdLostEvent(String message) {
        this.mMessage = message;
    }

    PwdLostEvent(Parcel source) {
        this.mMessage = source.readString();
    }

    public String getMessage() {
        return this.mMessage;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PARCEL
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

    public static final Creator<PwdLostEvent> CREATOR = new Creator<PwdLostEvent>() {
        @Override
        public PwdLostEvent createFromParcel(final Parcel source) {
            return new PwdLostEvent(source);
        }

        @Override
        public PwdLostEvent[] newArray(final int size) {
            return new PwdLostEvent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.mMessage);
    }
}
