package loicduparc.myapp.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Date;

public class Preferences {

    private static final String _SERVICE_OP_KEY_ = "MyApp:_SERVICE_OP:";

    public static void setAbTested(Context context, boolean value) {
        if (context != null) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putBoolean(_SERVICE_OP_KEY_, value);
            editor.apply();
        }
    }

    public static boolean isABTested(Context context) {
        if (context != null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            return prefs != null && prefs.getBoolean(_SERVICE_OP_KEY_, false);
        }
        return false;
    }


}
